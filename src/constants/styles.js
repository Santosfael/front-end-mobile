import { StyleSheet } from "react-native";
import Constants from "expo-constants";

export default StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 20,
    paddingTop: Constants.statusBarHeight + 10,
    alignItems: "center",
  },

  header: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },

  cicle: {
    marginTop: 50,
    width: 200,
    height: 200,
    borderRadius: 100,
    overflow: "hidden",
    margin: 15,
    resizeMode: "stretch",
    alignItems: "center",
  },

  buttons: {
    marginTop: 12,
    flexDirection: "row",
    borderRadius: 8,
    height: 50,
    width: "80%",
    justifyContent: "center",
    alignItems: "center",
  },

  textFont: {
    color: "#696F76",
    fontWeight: "bold",
    fontSize: 15,
  },

  gradient: {
    borderRadius: 8,
    flex: 1,
    height: 50,
    justifyContent: "center",
    alignItems: "center",
  },

  textButtons: {
    color: "#FFFFFF",
    fontSize: 15,
    fontWeight: "bold",
    fontFamily: "Roboto",
  },

  textCad: {
    marginTop: 170,
    color: "#969FAA",
    fontSize: 13,
    fontFamily: "Roboto",
    fontWeight: "normal",
    textAlign: "center",
  },

  textCad2: {
    color: "#969FAA",
    fontSize: 13,
    fontFamily: "Roboto",
    fontWeight: "bold",
  },

  textView: {
    justifyContent: "space-around",
  },

  textInput: {
    textAlign: "center",
    marginTop: 12,
    height: 45,
    width: "80%",
    borderRadius: 8,
    borderColor: "#00A6FF",
    borderWidth: 2,
    fontSize: 20,
    color: "#969FAA",
  },
});
