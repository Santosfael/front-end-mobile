import React, { useState, useEffect } from "react";
import { Feather } from "@expo/vector-icons";
import { useNavigation, useRoute } from "@react-navigation/native";
import {
  View,
  Image,
  TouchableOpacity,
  Text,
  TextInput,
  KeyboardAvoidingView,
  Platform,
  TouchableWithoutFeedback,
  Button,
  Keyboard,
} from "react-native";
import { LinearGradient } from "expo-linear-gradient";

import logoImg from "../../assets/logo.jpg";
import iconFace from "../../assets/icons/icon_face.png";
import iconGmail from "../../assets/icons/gmail.png";

import styles from "./styles";
import stylesRoot from "../../constants/styles";

/**
 * Atribuição dos icones usados no desenvolvimento do aplicativo
 * Ícones feitos por <a href="https://www.flaticon.com/br/autores/freepik" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/br/" title="Flaticon"> www.flaticon.com</a>
 */
export default function Login() {
  const navigation = useNavigation();

  const [valueEmail, email] = useState("usuario@gmail.com");
  const [valuePassword, password] = useState("********");

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS == "ios" ? "padding" : "height"}
      style={styles.container}
    >
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style={styles.inner}>
          <View style={stylesRoot.cicle}>
            <Image source={logoImg} />
          </View>

          <Text style={{ fontSize: 24, fontWeight: "bold", marginBottom: 12 }}>
            Bem vindo!
          </Text>

          <TextInput
            style={stylesRoot.textInput}
            email={(text) => email(text)}
            //placeholder="E-mail"
            value={valueEmail}
            autoCompleteType="email"
          />

          <TextInput
            style={stylesRoot.textInput}
            password={(text) => password(text)}
            value={valuePassword}
          />
          <TouchableOpacity style={stylesRoot.buttons} onPress={() => null}>
            <LinearGradient
              colors={["#93cbe0", "#00A6FF", "#00A6FF"]}
              style={stylesRoot.gradient}
            >
              <Text style={stylesRoot.textButtons}>Logar</Text>
            </LinearGradient>
          </TouchableOpacity>
        </View>
      </TouchableWithoutFeedback>
      <View>
        <TouchableOpacity>
          <Text style={stylesRoot.textCad}>
            Você não tem uma conta?
            <Text style={stylesRoot.textCad2}> Cadastrar</Text>
          </Text>
        </TouchableOpacity>
      </View>
    </KeyboardAvoidingView>
  );
}
/*/<TextInput
            style={stylesRoot.textInput}
            email={(text) => email(text)}
            //placeholder="E-mail"
            value={valueEmail}
            autoCompleteType="email"
          />

          <TextInput
            style={stylesRoot.textInput}
            password={(text) => password(text)}
            value={valuePassword}
          />*/
