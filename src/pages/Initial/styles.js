import { StyleSheet } from "react-native";
import Constants from "expo-constants";

export default StyleSheet.create({
  buttonConectFace: {
    marginTop: 24,
    flexDirection: "row",
    borderRadius: 8,
    height: 50,
    width: "80%",
    justifyContent: "space-around",
    alignItems: "center",
    borderColor: "#00A6FF",
    borderWidth: 2,
    paddingLeft: 20,
    paddingRight: 20,
  },

  icon: {
    height: 25,
    width: 25,
  },

  buttonConectGoogle: {
    marginTop: 12,
    flexDirection: "row",
    borderRadius: 8,
    height: 50,
    width: "80%",
    justifyContent: "space-around",
    alignItems: "center",
    borderColor: "#00A6FF",
    borderWidth: 2,
    paddingLeft: 20,
    paddingRight: 20,
  },
});
