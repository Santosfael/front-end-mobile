import React, { useState, useEffect } from "react";
import { Feather } from "@expo/vector-icons";
import { useNavigation, useRoute } from "@react-navigation/native";
import { View, Image, TouchableOpacity, Text, Linking } from "react-native";
import { LinearGradient } from "expo-linear-gradient";

import logoImg from "../../assets/logo.jpg";
import iconFace from "../../assets/icons/icon_face.png";
import iconGmail from "../../assets/icons/gmail.png";

import styles from "./styles";
import stylesRoot from "../../constants/styles";

/**
 * Atribuição dos icones usados no desenvolvimento do aplicativo
 * Ícones feitos por <a href="https://www.flaticon.com/br/autores/freepik" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/br/" title="Flaticon"> www.flaticon.com</a>
 */
export default function Initial() {
  const navigation = useNavigation();
  return (
    <View style={stylesRoot.container}>
      <View style={stylesRoot.cicle}>
        <Image source={logoImg} />
      </View>
      <TouchableOpacity style={styles.buttonConectFace}>
        <Image style={styles.icon} source={iconFace} />
        <Text style={stylesRoot.textFont}>Conecte com o Facebook</Text>
      </TouchableOpacity>

      <TouchableOpacity style={styles.buttonConectGoogle}>
        <Image style={styles.icon} source={iconGmail} />
        <Text style={stylesRoot.textFont}>Conecte com o Gmail</Text>
      </TouchableOpacity>

      <TouchableOpacity
        style={stylesRoot.buttons}
        onPress={() => navigation.navigate("Login")}
      >
        <LinearGradient
          colors={["#93cbe0", "#00A6FF", "#00A6FF"]}
          style={stylesRoot.gradient}
        >
          <Text style={stylesRoot.textButtons}>Login</Text>
        </LinearGradient>
      </TouchableOpacity>
      <TouchableOpacity>
        <Text style={stylesRoot.textCad}>
          Você não tem uma conta?
          <Text style={stylesRoot.textCad2}> Cadastrar</Text>
        </Text>
      </TouchableOpacity>
    </View>
  );
}
